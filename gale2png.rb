# encoding: utf-8
# frozen_string_literal: true
require 'zlib'
require 'xml/mapping'


class GaleFile

    class GaleError < StandardError ;end

    class Layer
        include XML::Mapping
        text_node :name,"@Name"
        numeric_node :trans_color,"@TransColor"
        numeric_node :alpha,"@Alpha"
        boolean_node :visible,'@Visible','1','0'
        object_node :chunk_index,".",unmarshaller: ->(e){REXML::XPath.first(e,'count(./preceding::Layer)')*2+1}
        private :chunk_index=

    end

    class Frame
        include XML::Mapping
        text_node :name,"@Name"
        numeric_node :trans_color,"@TransColor"
        numeric_node :delay,"@Delay"
        object_node :palette,"Layers/RGB",unmarshaller: ->(e){e.get_text.value.scan(/.{6}/).map{|c|c.to_i 16}},optional: true

        array_node :layers,"Layers/Layer",class: Layer
    end


    include XML::Mapping
    numeric_node :version,'@Version'
    numeric_node :width,'@Width'
    numeric_node :height,'@Height'
    numeric_node :bpp,'@Bpp'
    numeric_node :bg_color,'@BGColor'
    boolean_node :not_fill_bg,'@NotFillBG','1','0'

    array_node :frames,"Frame",class: Frame

    attr_reader :chunks

    def render_frame(index)
        buffer_size = width*height
        buffer = [not_fill_bg ? 0 : bg_color|0xFF_00_00_00]*buffer_size
        frame = frames[index]
        frame.layers.each do |layer|
            next unless layer.visible
            pixels = nil
            raise "Chunk #{layer.chunk_index} does not exist (count: #{chunks.count})" if layer.chunk_index >= chunks.count
            chunk = chunks[layer.chunk_index]
            case bpp
            when 8
                raise GaleError,"Inavlid chunk size (#{chunk.size}, expected #{buffer_size}" if chunk.size != buffer_size
                chunk.each_byte.with_index do |pixel,i|
                    next if pixel==layer.trans_color
                    buffer[i] = frame.palette[pixel]|0xFF_00_00_00
                end
            else raise "#{bpp} bits per pixel not implemented"
            end
        end
        return buffer.pack('L<*')
    end

    # Overrides definition from XML lib
    def self.load_from_file(filename)
        obj = nil
        chunks = []
        File.open filename,'rb' do |f|
            magic = f.read(8)
            raise GaleError,"Not a GraphicsGale File (magic is #{magic.inspect})" unless magic == "GaleX200"
            until f.eof?
                chunksize = f.read(4).unpack("L<")[0]
                if chunksize > 0
                    chunkdata = Zlib.inflate(f.read(chunksize)).freeze
                    chunks << chunkdata
                else
                    chunks << "".b
                end
            end
            puts "Loaded #{chunks.count} chunks"
        end
        obj = load_from_xml(REXML::Document.new(chunks[0].dup.force_encoding(Encoding::Windows_1250).encode(Encoding::UTF_8)).root)
        obj.send(:chunks=,chunks)
        return obj
    end

    private
    attr_writer :chunks
end


if $0 == __FILE__

    raise unless ARGV.size == 1

    data = GaleFile.load_from_file ARGV[0]
    #p data
    #p data.frames[-1].layers[-1].chunk_index

    require 'chunky_png'
    
    img = ChunkyPNG::Image.from_rgba_stream(data.width,data.height,data.render_frame(0))
    img.save(ARGV[0]+".png")
end

